<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'nucleargauges');



/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8hoNP}cw?)Q}T).{,[/m6G6&Z@*/uvPmMBk^p j*O@Qn_~)J`GwX33>Z_2{$2`u*');
define('SECURE_AUTH_KEY',  ',*$BBXPV-MrO&|P:pt3^E>h*0B8Ezk3woP}A~F$<q7 &ofQ]EMCyU&t5-o3/BdBx');
define('LOGGED_IN_KEY',    '+M>Oj(wAeo |4N`IwU(hwpZu2<0S0H!512CN|Lv@g&uv$fJ~1jgY?mkHYE]i6%Ha');
define('NONCE_KEY',        '~7oy.~iuQ=!#q$Mw!dSwlO2R.u$N(;V{G$fm2.*;$y;Qb<n)^Aj|v}udZzy<dJco');
define('AUTH_SALT',        '(?sp:2~D$Tj]Gen0nD`Bu~X+a2(/~Z94TMs*-FdFKc^[3c;=i0+MIs2<km~|X BT');
define('SECURE_AUTH_SALT', 'fNpP)3fBJN8K@_A^H-c`FnL?|$5g(.>=d9?&hZov`h =1~!:MA+/&jz@$q|Wd0=[');
define('LOGGED_IN_SALT',   '`beP)[Ba=DiNRUA,F`r[*t_v*P,wcDoP^ahg |@n}6yep@3.ql*48~@v0YtqfaMS');
define('NONCE_SALT',       'HN:M2}<3u&X43^7Wd|%LE(92j< 4e)p0vikoR:(U]K#St&AqS-/RiFT7Aqu}E#X!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
